
def indexeur(nom_fichier, mots) :

    with open(nom_fichier) as fichier:
        index = {}

        for numero, ligne in enumerate(fichier, 1) :
            mots_ligne = set(ligne.split()) # on fait un ensemble des mots de la ligne, pour ne pas avoir les doublons

            for m in mots & mots_ligne : # intersection d'ensemble
                index.setdefault(m, set())# si m est déjà dans index, le rajoute, sinon, le crée
                index[m].add(numero)

    return index


def main():

        # Définir le nom du fichier
    erreur_fichier = False

    while True :
        import sys
        if not erreur_fichier and len(sys.argv) > 2: # via la ligne de commande
            
            nom = sys.argv[1] 
            mots = set(sys.argv[2:])   # le nom du fichier
        else:                  # via l'utilisateur du programme
            nom = input('Nom du fichier : ').strip()
            mots = set(sys.argv[2:]) 

        try:
            resultats = indexeur(nom, mots)
            resultats
            for mot, lignes in resultats.items():
                lignes = list(lignes)
                print(f"{mot} : {', '.join([str(x) for x in lignes])}")
            return

        except FileNotFoundError:
            erreur_fichier = True
            print("le fichier n'existe pas")

        except Exception as e:
            print("Probleme :", e)
            return

if __name__ == '__main__':
    main()